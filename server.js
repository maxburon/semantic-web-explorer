const fs = require('fs')
const http = require('http')
const request = require('request')
const preq = require('preq')
const cheerio = require('cheerio')
const parseAll = require('html-metadata').parseAll
const RdfaParser = require('rdfa-streaming-parser').RdfaParser
const OGParser = require('./libs/parser-open-graph')
const SchemaOrgParser = require('./libs/parser-schemaOrg')
const QuadToTripleTransform = require('./libs/transform-quad-to-triple')

const PORT = process.env.PORT || 8100

http.createServer((req, res) => {
  const baseURL = 'http://' + req.headers.host + '/'
  const url = new URL(req.url, baseURL)

  if (url.pathname === '' || url.pathname === '/') {
    const indexHTML = fs.readFileSync('index.html')
    res.setHeader('Content-Type', 'text/html; charset=utf-8')
    return res.end(indexHTML)
  }

  if (url.pathname === '/index.js') {
    const indexJS = fs.readFileSync('index.js')
    res.setHeader('Content-Type', 'application/javascript; charset=utf-8')
    return res.end(indexJS)
  }

  if (url.pathname === '/lib.js') {
    const libJS = fs.readFileSync('lib.js')
    res.setHeader('Content-Type', 'application/javascript; charset=utf-8')
    return res.end(libJS)
  }

  if (url.pathname === '/jsonld' &&
      url.searchParams.has('url')) {
    res.setHeader('Content-Type', 'application/json; charset=utf-8')
    const urlToScrape = url.searchParams.get('url').toString()
    console.log(urlToScrape)

    const options = {
      url: urlToScrape,
      jar: request.jar(), // Cookie jar
      headers: {
        'User-Agent': 'webscraper'
      }
    }

    return preq(options).then(response => {
      const $ = cheerio.load(response.body)
      return parseAll($).then(metadata => {
//        console.log('meta\n ', JSON.stringify(metadata.schemaOrg))


        let p = new Promise((resolve, reject) => {
          resolve(rdf.dataset())
        })

        if (metadata.jsonLd) {
          p = parseJSONLD(metadata.jsonLd)
        }

        if (metadata.openGraph) {
          p = p.then(d => OGParser.parse(metadata.openGraph)
                     .then(ogd => d.addAll(ogd)))
        }

        if (metadata.schemaOrg) {
          p = p.then(d => SchemaOrgParser.parse(metadata.schemaOrg)
                     .then(ogd => d.addAll(ogd)))
        }

        return p
      })
        .then(d => new Promise((resolve, reject) => {
          const rdfaParser = new RdfaParser({ baseIRI: 'http://example.com', contentType: 'text/html' })
          rdfaParser
            .on('data', quad => d.add(rdf.quad(quad.subject, quad.predicate, quad.object)))
            .on('error', reject)
            .on('end', () => resolve(d))

//          rdfaParser.write(response.body)
          rdfaParser.end()
        }))
        .then(d => {
          let quadToTriple = new QuadToTripleTransform()
          rdf.dataset().import(d.toStream().pipe(quadToTriple))
            .then(d => deriveWebPageURL(d))
            .then(d => {
              buildEquivalentTerms(d, [rdf.namedNode('http://schema.org/url'), rdf.namedNode('http://www.w3.org/1999/02/22-rdf-syntax-ns#type')])
              buildEquivalentTerms(d, [rdf.namedNode('http://schema.org/name'), rdf.namedNode('http://www.w3.org/1999/02/22-rdf-syntax-ns#type')])
              console.log(d.toString())
              return serializeDataset(d)
                .then(s => {
                  s.on('data', j => res.write(j))
                  s.on('end', () => res.end())
                })
            })
        })
        .catch(e => {
          res.statusCode = 500
          console.error("error\n" + e)
          res.end()
        })
    })
  }
  res.setHeader('Content-Type', 'text/plain; charset=utf-8')
  res.statusCode = 404
  return res.end()
}).listen(PORT)

// ----- utils -----

const rdf = require('rdf-ext')
const ParserJsonld = require('@rdfjs/parser-jsonld')
const Readable = require('stream').Readable

function parseJSONLD (jsonLd) {
  return new Promise((resolve, reject) => {
    const jsonldParser = new ParserJsonld()
    const input = new Readable({
      read: () => {
        let s = JSON.stringify(jsonLd)
        input.push(s)
        input.push(null)
      }
    })

    const dataset = rdf.dataset()
    jsonldParser.import(input)
      .on('data', quad => {
        dataset.add(quad)
      })
      .on('end', () => {
        resolve(dataset)
      }).on('error', e => {
        console.log(e)
        resolve(dataset)
      })
  })
}

const TripleSetsSerializer = require('./libs/triple-set-serialiazer')

function serializeDataset (dataset) {
  const serializer = new TripleSetsSerializer()
  return new Promise((resolve, reject) => {
    const input = dataset.toStream()
    resolve(serializer.import(input))
  })
}

// ----- Mining -----

// **** url of webPage *****
// if the IRI is a webpage, then its url is the IRI

function deriveWebPageURL (dataset) {
  return new Promise((resolve, reject) => {
    // select web pages
    const urlPredicate = rdf.namedNode('http://schema.org/url')
    const webpageIRI = rdf.namedNode('http://schema.org/WebPage')
    const prop = rdf.namedNode('http://www.w3.org/1999/02/22-rdf-syntax-ns#type')
    const webpages = dataset.match(null, prop, webpageIRI)
          .filter(quad => quad.subject.termType === 'NamedNode')
          .toArray()

    for (let q of webpages) {
      const quad = rdf.quad(q.subject, urlPredicate, rdf.literal(q.subject.value))
      dataset.add(quad)
    }

    const quadsWithURL = dataset.match(null, rdf.namedNode('http://ogp.me/ns#url')).toArray()
    for (let q of quadsWithURL) {
      const quad = rdf.quad(q.subject, urlPredicate, q.object)
      dataset.add(quad)
    }

    resolve(dataset)
  })
}

// ***** Transitivity ********

function starJoinQuery (dataset, predicates) {
  const pAnswers = []
  for (let p of predicates) {
    pAnswers.push(dataset.match(null, p))
  }
  let answers = (pAnswers.length) ? pAnswers[0].toArray().map(q => [q]) : []
  
  for (let i = 1; i < predicates.length; i++) {
    const tmp = []
    for (let a of answers) {
      const subject = a[0].subject
      const nextAnswers = pAnswers[i].match(subject)
      for (let b of nextAnswers.toArray()) {
        const joinAnswer = a.slice()
        joinAnswer.push(b)
        tmp.push(joinAnswer)
      }
    }
    answers = tmp
  }

  return answers
}

function groupSubjectByCommonObjects (joinAnswers) {
  return joinAnswers
    .reduce((acc, quads) => {
      const key = quads.reduce((acc, quad) => {
        return acc + '|' + quad.object.termType + '$' + quad.object.value
      }, '')

      if (acc[key] !== undefined) {
        acc[key].push(quads[0].subject)
      } else {
        acc[key] = [quads[0].subject]
      }
      return acc
    }, {})
}

function buildEquivalentTerms (dataset, predicates) {
  const joinAnswers = starJoinQuery(dataset, predicates)

  const subjectsPerObject = groupSubjectByCommonObjects(joinAnswers)

  for (let object in subjectsPerObject) {
    const subjects = subjectsPerObject[object]
    if (subjects.length > 1) {
      const subIRIs = subjects.filter(s => s.termType === 'NamedNode')
      const repr = (subIRIs.length) ? subIRIs[0] : subjects[0]

      for (let s of subjects) {
        if (s !== repr) {
          const answers = dataset.match(s, null, null).toArray()
          const modified = answers.map(q => {
            return rdf.quad(repr, q.predicate, q.object, q.graph)
          })
          dataset.addAll(modified)

          for (let a of answers) {
            dataset.delete(a)
          }

          const oAnswers = dataset.match(null, null, s).toArray()
          const oModified = oAnswers.map(q => {
            return rdf.quad(q.subject, q.predicate, repr, q.graph)
          })
          dataset.addAll(oModified)

          for (let a of oAnswers) {
            dataset.delete(a)
          }
        }
      }
    }
  }
}

const rdf = require('rdf-ext')

function parseSchemaOrg (schemaOrg, g) {
  const dataset = rdf.dataset()
  const graph = g || rdf.namedNode('sch:1')

  return new Promise((resolve, reject) => {
    const context = 'http://schema.org/'
    function importSchemaOrgRec (schOrg, s) {
      const subject = rdf.blankNode()

      for (let type of schOrg.type) {
        const prop = rdf.namedNode('http://www.w3.org/1999/02/22-rdf-syntax-ns#type')
        const object = rdf.namedNode(type)
        const quad = rdf.quad(subject, prop, object, graph)
        dataset.add(quad)
      }

      for (let p in schOrg.properties) {
        for (let value of schOrg.properties[p]) {
          if (typeof value === 'string') {
            const prop = rdf.namedNode(context + p)
            const object = (value.startsWith('https://')) ? rdf.namedNode(value) : rdf.literal(value)
            const quad = rdf.quad(subject, prop, object, graph)
            dataset.add(quad)
          }

          if (typeof value === 'object') {
            const prop = rdf.namedNode(context + p)
            const object = importSchemaOrgRec(value)
            const quad = rdf.quad(subject, prop, object, graph)
            dataset.add(quad)
          }
        }
      }
      return subject
    }

    for (let item of schemaOrg.items) {
      importSchemaOrgRec(item)
    }

    resolve(dataset)
  })
}

module.exports.parse = parseSchemaOrg

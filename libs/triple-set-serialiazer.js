const Sink = require('@rdfjs/sink')
const Readable = require('stream').Readable

class TripleSetsSerializer extends Sink {
  constructor () {
    super(TripleSetsSerializerStream)
  }
}

// https://www.w3.org/wiki/JSON_Triple_Sets
class TripleSetsSerializerStream extends Readable {
  constructor (input) {
    super({
      read: () => {}
    })
    let first = true

    function termEscapedValue (term, graph) {
      if (term.termType === 'BlankNode') {
        return graph.value + term.value
      }
      return term.value
    }

    this.push('[')
    input.on('data', quad => {
      const j = {
        s: termEscapedValue(quad.subject, quad.graph),
        s_type: termTypeTranslation(quad.subject.termType),
        p: termEscapedValue(quad.predicate, quad.graph),
        p_type: termTypeTranslation(quad.predicate.termType),
        o: termEscapedValue(quad.object, quad.graph),
        o_type: termTypeTranslation(quad.object.termType)
      }
      if (first) {
        first = false
        this.push(JSON.stringify(j))
      } else {
        this.push(', ' + JSON.stringify(j))
      }
    })

    input.on('end', () => {
      this.push(']')
      this.push(null)
    })

    input.on('error', err => {
      this.emit('error', err)
    })
  }
}

function termTypeTranslation (termtype) {
  switch (termtype) {
    case 'NamedNode':
      return 'uri'
    case 'BlankNode':
      return 'bnode'
    case 'Literal':
      return 'literal'
    default:
      throw new Error('UnSupported termtype ' + termtype)
  }
}

module.exports = TripleSetsSerializer

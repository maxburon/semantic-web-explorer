const rdf = require('rdf-ext')

function parseOpenGraph (og, g) {
  const dataset = rdf.dataset()
  const graph = g || rdf.namedNode('og:1')

  return new Promise((resolve, reject) => {
    const context = 'http://ogp.me/ns#'
    function importOpenGraphRec (cog, s) {
      const subject = rdf.blankNode()
      for (let key in cog) {
        if (key === 'type') {
          const prop = rdf.namedNode('http://www.w3.org/1999/02/22-rdf-syntax-ns#type')
          const object = rdf.namedNode(context + cog[key])
          const quad = rdf.quad(subject, prop, object, graph)
          dataset.add(quad)
          continue
        }

        if (typeof cog[key] === 'string') {
          const prop = rdf.namedNode(context + key)
          const object = (cog[key].startsWith('https://')) ? rdf.namedNode(cog[key]) : rdf.literal(cog[key])
          const quad = rdf.quad(subject, prop, object, graph)
          dataset.add(quad)
        }

        if (typeof cog[key] === 'object') {
          const prop = rdf.namedNode(context + key)
          const object = importOpenGraphRec(cog[key])
          const quad = rdf.quad(subject, prop, object, graph)
          dataset.add(quad)
        }
      }
      return subject
    }

    importOpenGraphRec(og)
    resolve(dataset)
  })
}

module.exports.parse = parseOpenGraph

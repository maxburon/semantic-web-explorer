const rdf = require('rdf-ext')
const Transform = require('readable-stream').Transform

class QuadToTripleTransform extends Transform {
  constructor (graph, options) {
    super()

    options = options || {}

    this._writableState.objectMode = true
    this._readableState.objectMode = true

    this.factory = options.factory || rdf
    this.graph = graph || rdf.defaultGraph()
    this.bnCount = 0
    this.bnIndex = {}

    this.on('pipe', (input) => {
      input.on('error', (err) => {
        this.emit('error', err)
      })
    })
  }

  term (t, g) {
    switch (t.termType) {
    case 'BlankNode':
      if (!this.bnIndex[g.value]) {
        this.bnIndex[g.value] = {}
      }

      if (this.bnIndex[g.value][t.value] === undefined) {
        const bn = this.factory.blankNode(this.bnCount++)
        this.bnIndex[g.value][t.value] = bn
      }
      
      return this.bnIndex[g.value][t.value]
    default:
      return t
    }
  }

  _transform (quad, encoding, done) {
    const subject = this.term(quad.subject, quad.graph)
    const predicate = this.term(quad.predicate, quad.graph)
    const object = this.term(quad.object, quad.graph)
    this.push(this.factory.quad(subject, predicate, object, this.graph))

    done()
  }
}

module.exports = QuadToTripleTransform
